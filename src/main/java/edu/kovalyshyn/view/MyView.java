package edu.kovalyshyn.view;

import edu.kovalyshyn.controller.Controller;
import edu.kovalyshyn.controller.ControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private static Logger log = LogManager.getLogger(MyView.class);


    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", " 1 - get Value from Tree by Key");
        menu.put("2", " 2 - remove Node");
        menu.put("3", " 3 - put new Value");
        menu.put("4", " 4 - print tree map");
        menu.put("Q", " Q - EXIT");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);

    }

    private void pressButton1() {
        log.info("Enter key");
        int key = input.nextInt();
        log.info(controller.getValueByKey(key));

    }

    private void pressButton2() {
        log.info("Enter key");
        int key = input.nextInt();
        controller.removeNode(key);
    }

    private void pressButton3() {
        log.info("Enter new value");
        String value = input.nextLine();
        log.info("Input key");
        int key = input.nextInt();
        controller.putNewNode(key, value);
    }
    private void pressButton4() {

        controller.print();
    }



    private void outputMenu() {
        System.out.println("\n MENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                log.info("-EXIT- ");
            }
        } while (!keyMenu.equals("Q"));
    }
}
