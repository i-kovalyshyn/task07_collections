package edu.kovalyshyn.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class SimpleBinaryTreeMap<K extends Comparable, V> implements Map<K, V> {
    private static Logger log = LogManager.getLogger(SimpleBinaryTreeMap.class);

    private int size = 0;
    private Node<K, V> root;

    //The Node Class
    private static class Node<K, V> implements Map.Entry<K, V> {
        private Node<K, V> left;
        private Node<K, V> right;
        private K key;
        private V value;

        public Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            V oldValue = this.value;
            this.value = value;
            return oldValue;
        }

        public void print() {
            log.info("Key: " + key + " Value: " + value);
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        return false;
    }

    @Override
    public V get(Object key) {
        Comparable<? super K> c = (Comparable<? super K>) key;
        Node<K, V> node = root;
        while (node != null) {
            int cmp = c.compareTo(node.key);
            if (cmp < 0) {
                node = node.left;
            } else if (cmp > 0) {
                node = node.right;
            } else {
                return node.value;
            }
        }
        return null;
    }

    @Override
    public V put(K key, V value) {
        Node<K, V> parent = root;
        Node<K, V> temp = root;
        if (root == null) {
            root = new Node<>(key, value);
        } else {
            int cmp;
            do {
                parent = temp;
                cmp = key.compareTo(temp.key);
                if (cmp < 0) {
                    temp = temp.left;
                } else if (cmp > 0) {
                    temp = temp.right;
                } else {
                    V oldValue = temp.value;
                    temp.value = value;
                    return oldValue;
                }
            } while (temp != null);
            if (cmp < 0) {
                parent.left = new Node<>(key, value);
            } else if (cmp > 0) {
                parent.right = new Node<>(key, value);
            }
            size++;
        }
        return null;
    }

    @Override
    public V remove(Object key) {
        K k = (K) key;
        Node<K, V> removingNode = root;
        Node<K, V> parent = root;
        Node<K, V> successor = null;
        if (root == null) {
            return null;
        } else {
            int cmp;
            do {
                cmp = k.compareTo(removingNode.key);
                if (cmp < 0) {
                    if (removingNode.left == null) return null;
                    parent = removingNode;
                    removingNode = removingNode.left;
                } else if (cmp > 0) {
                    if (removingNode.right == null) return null;
                    parent = removingNode;
                    removingNode = removingNode.right;
                } else {
                    break;
                }
            } while (true);

            if (removingNode.right == null && removingNode.left == null) {
                if (root == removingNode) root = null;
                else if (parent.left == removingNode) parent.left = null;
                else if (parent.right == removingNode) parent.right = null;
            } else if (removingNode.right == null) {


                if (parent.left == removingNode) parent.left = removingNode.left;
                else parent.right = removingNode.left;
            } else if (removingNode.left == null) {
                if (parent.left == removingNode) parent.left = removingNode.right;
                else parent.right = removingNode.right;
            } else {
                successor = getSuccessor(removingNode);

                if (removingNode == root) {
                    root = successor;
                } else if (parent.left == removingNode) {
                    parent.left = successor;
                } else {
                    parent.right = successor;
                    successor.left = removingNode.left;
                }
            }
            size--;
        }
        return removingNode.getValue();
    }

    private Node<K, V> getSuccessor(Node<K, V> removingNode) {
        Node<K, V> successorParent = removingNode;
        Node<K, V> successor = removingNode;
        Node<K, V> current = removingNode.right;

        while (current != null) {
            successorParent = successor;
            successor = current;
            current = current.left;
        }
        if (successor != removingNode.right) {
            successorParent.left = successor.right;
            successor.right = removingNode.right;
        }
        return successor;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {

    }

    @Override
    public void clear() {

    }

    @Override
    public Set<K> keySet() {
        Set<K> set = new HashSet<>();
        runTreeForKey(root, set);
        return set;
    }

    public void runTreeForKey(Node<K, V> localRoot, Set<K> set) {
        if (localRoot != null) {
            runTreeForKey(localRoot.left, set);
            set.add(localRoot.getKey());
            runTreeForKey(localRoot.right, set);
        }
    }

    @Override
    public Collection<V> values() {
        Collection<V> array = new ArrayList<>();
        runTreeForValue(root, array);
        return array;
    }

    public void runTreeForValue(Node<K, V> localRoot, Collection<V> array) {
        if (localRoot != null) {
            runTreeForValue(localRoot.left, array);
            array.add(localRoot.getValue());
            runTreeForValue(localRoot.right, array);
        }
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        Set<Entry<K, V>> set = new HashSet<>();
        runTreeForNode(root, set);
        return set;
    }

    public void runTreeForNode(Node<K, V> localRoot, Set<Entry<K, V>> set) {
        if (localRoot != null) {
            runTreeForNode(localRoot.left, set);
            set.add(localRoot);
            runTreeForNode(localRoot.right, set);
        }
    }


    public void displayTree() {
        Stack globalStack = new Stack();
        globalStack.push(root);
        int nBlanks = 35;
        boolean isRowEmpty = false;
        log.info(
                "................................................................................");
        while (isRowEmpty == false) {
            Stack localStack = new Stack();
            isRowEmpty = true;
            for (int j = 0; j < nBlanks; j++)
                System.out.print(' ');
            while (globalStack.isEmpty() == false) {
                Node temp = (Node) globalStack.pop();
                if (temp != null) {
                    System.out.print(temp.value);
                    localStack.push(temp.left);
                    localStack.push(temp.right);
                    if (temp.left != null ||
                            temp.right != null)
                        isRowEmpty = false;
                } else {
                    System.out.print("--");
                    localStack.push(null);
                    localStack.push(null);
                }
                for (int j = 0; j < nBlanks * 2 - 2; j++)
                    System.out.print(' ');
            }
            log.info("\n");
            nBlanks /= 2;
            while (localStack.isEmpty() == false)
                globalStack.push(localStack.pop());
        }
        log.info(
                "...........................................................................");
    }

    public void print() {
        inOrder(root);
    }

    private void inOrder(Node localRoot) {
        if (localRoot != null) {
            inOrder(localRoot.left);
            log.info("Key: " + localRoot.key + " Value: " + localRoot.value);
            inOrder(localRoot.right);
        }
    }


}
