package edu.kovalyshyn.model;

public class ModelImpl implements Model {
    private SimpleBinaryTreeMap<Integer, String> treeMap;

    public ModelImpl() {
        treeMap = new SimpleBinaryTreeMap<>();
        treeMap.put(65, "A");
        treeMap.put(77, "M");
        treeMap.put(89, "Y");
        treeMap.put(76, "L");
        treeMap.put(66, "B");
        treeMap.put(70, "F");
        treeMap.put(68, "D");
        treeMap.put(79, "O");
        treeMap.put(75, "K");
        treeMap.put(82, "R");
        treeMap.put(72, "H");
        treeMap.put(84, "T");
        treeMap.put(74, "J");
    }

    @Override
    public void putNewNode(int key, String value) {
        treeMap.put(key, value);
        treeMap.print();
    }

    @Override
    public void removeNode(int key) {
        treeMap.remove(key);
        treeMap.print();
    }

    @Override
    public String getValueByKey(int key) {
        return treeMap.get(key);
    }

    @Override
    public void print() {
        treeMap.print();
    }
}
